# dotfiles :rainbow: :unicorn:

Hello! My name is Brie and these are my dotfiles. 

## ~brie/.screenrc
I like to enable the statusbar in `screen` and view the date, time, hostname and system load averages. 

### Bonus: read changes to .screenrc without restarting screen
It's true! You can pick up changes to your `.screenrc` **without** exiting your current `screen` session! Here's how: 

  * `Ctrl` + `a`
  * `: source ~/.screenrc`

[source](https://serverfault.com/a/194612) 

[![asciicast](https://asciinema.org/a/laQx7EU7vAao9KSYuu4nUbwFo.svg)](https://asciinema.org/a/laQx7EU7vAao9KSYuu4nUbwFo)

## ~brie/.wgetrc
The [.wgetrc](wgetrc) file in this repository contains excerpts I like from [Mathias Bynens](https://github.com/mathiasbynens/dotfiles/blob/master/.wgetrc).
